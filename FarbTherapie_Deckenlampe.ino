#include <Arduino.h>
#include <FastLED.h>
#include <TimedAction.h>
#include <EEPROM.h>
#include <Debounce.h>


/***********************************************************
 *
 * Farbtherapie - Deckenlampe
 *
 * Feste Farbwerte, einstellbar über Taster
 * maximale Leuchtstärke (nicht dimmbar)
 *
 * Autor:	LunaX
 *
 * History:
 * 07-12-2015		V01		initial
 * 12-12-2015		V02		update to 72 LEDs
 *
 ***********************************************************/


// FastLED
#define LED_PIN 	3
#define	CHIPSET		NEOPIXEL
#define NUM_LEDS	72
#define DEFAULT_BRIGHTNESS_MAX 0xFE
#define DEFAULT_BRIGHTNESS_MIN 50
#define FRAMES_PER_SECOND 30

// EEPROM
#define EEPROM_SAVE_MODE 0
#define EEPROM_SAVE_DIY1 1
#define EEPROM_SAVE_DIY2 2
#define EEPROM_SAVE_DIY3 3
#define EEPROM_SAVE_GLITTER 20
#define EEPROM_SAVE_TIMEOUT 5000

#define BUTTON_PIN 2

CRGB leds[NUM_LEDS];

int16_t lastMillis;

uint8_t gCurrentPatternNumber = 0; // Index number of which pattern is current
uint8_t gCurrentBrightness = DEFAULT_BRIGHTNESS_MAX;

uint8_t gMode = 1;
bool isSomethingChanged = false;

Debounce debouncer = Debounce( 80 , BUTTON_PIN );

TimedAction save = TimedAction(EEPROM_SAVE_TIMEOUT, SaveEEProm);

typedef void (*MultipleArgumentPattern)(uint8_t arg0, uint8_t arg1,
		uint8_t arg2);
typedef struct {
	MultipleArgumentPattern mPattern;
	uint8_t mArg0;
	uint8_t mArg1;
	uint8_t mArg2;
} MultipleArgumentPatterWithArgumentValues;

MultipleArgumentPatterWithArgumentValues gPatternsAndArguments[] = {
		{ FixAmbient, 0, 255, 255 },	// red
		{ FixAmbient, 32, 255, 255 },	// orange
		{ FixAmbient, 64, 255, 255 },	// yellow
		{ FixAmbient, 96, 255, 255 },	// green
		{ FixAmbient, 128, 255, 255 },	// aqua
		{ FixAmbient, 160, 255, 255 },	// blue
		{ FixAmbient, 192, 255, 255 },	// purple
		{ FixAmbient, 224, 255, 255 },	// pink
		};

//The setup function is called once at startup of the sketch
void setup() {
	// initialize debug leds and both 3x blink for 200ms
	pinMode(BUTTON_PIN,INPUT);
	digitalWrite(BUTTON_PIN, HIGH);
	// initialize timer for button

	FastLED.addLeds<CHIPSET, LED_PIN>(leds, NUM_LEDS).setCorrection(
			TypicalLEDStrip).setDither(DEFAULT_BRIGHTNESS_MAX < 255);
	FastLED.setBrightness( DEFAULT_BRIGHTNESS_MAX);

	gCurrentPatternNumber = EEPROM.read(EEPROM_SAVE_DIY1);
	gCurrentBrightness = EEPROM.read(EEPROM_SAVE_DIY2);
	if (gCurrentBrightness == 0xFF)
		gCurrentBrightness = DEFAULT_BRIGHTNESS_MAX;
	if (gCurrentPatternNumber == 0xFF)
		gCurrentPatternNumber = 0;


//	gCurrentPatternNumber = 7;
//	gCurrentBrightness = 255;
//	gHue = 0;
	FastLED.setBrightness( gCurrentBrightness);
	isSomethingChanged = false;
	save.disable();
}

// The loop function is called in an endless loop
void loop() {
	debouncer.update();
	save.check();
	if (debouncer.read() == 0 ) {
		save.reset();
		nextPattern();
		isSomethingChanged = true;
		save.enable();
	}
	if (!isSomethingChanged) {
		save.disable();
		save.reset();
	}
	// Add entropy to random number generator; we use a lot of it.
	random16_add_entropy(random());

	// Call the current pattern function once, updating the 'leds' array
	uint8_t arg0 = gPatternsAndArguments[gCurrentPatternNumber].mArg0;
	uint8_t arg1 = gPatternsAndArguments[gCurrentPatternNumber].mArg1;
	uint8_t arg2 = gPatternsAndArguments[gCurrentPatternNumber].mArg2;

	MultipleArgumentPattern pat =
			gPatternsAndArguments[gCurrentPatternNumber].mPattern;

	pat(arg0, arg1, arg2);

	// send the 'leds' array out to the actual LED strip
	FastLED.show();

#if defined(FASTLED_VERSION) && (FASTLED_VERSION >= 2001000)
	FastLED.delay(1000 / FRAMES_PER_SECOND);
#else
	delay(1000 / FRAMES_PER_SECOND);
#endif  ﻿

}

void nextPattern() {
	// add one to the current pattern number, and wrap around at the end
	const int numberOfPatterns = sizeof(gPatternsAndArguments)
			/ sizeof(gPatternsAndArguments[0]);
	gCurrentPatternNumber = (gCurrentPatternNumber + 1) % numberOfPatterns;
}

//
// timer interrupt, used from encoder
//
void timerIsr() {

}

void FlashStripe(uint8_t count, CRGB c) {
	for (int x = 0; x < NUM_LEDS; x++) {
		leds[x] = c;
	}
	for (int x = 0; x < count; x++) {
		FastLED.show();
		FastLED.delay(250);
	}
	FastLED.clear(false);
	delay(100);
}

void SaveEEProm() {
	if (isSomethingChanged) {
		EEPROM.write(EEPROM_SAVE_DIY1, gCurrentPatternNumber);
		EEPROM.write(EEPROM_SAVE_DIY2, gCurrentBrightness);
		for (int x=0; x < NUM_LEDS; x++) {
			leds[random16(NUM_LEDS)] = CRGB::White;
			FastLED.show();
			delay(10);
		}
		save.reset();
		save.disable();
		isSomethingChanged = false;
	}
}

//
// Farbtherapie - Pattern
void FixAmbient(uint8_t arg0, uint8_t arg1, uint8_t arg2) {
	// arg0 = color
	fill_solid(leds, NUM_LEDS, CHSV(arg0, arg1, arg2));

}
