//This is a automatic generated file
//Please do not modify this file
//If you touch this file your change will be overwritten during the next build
//This file has been generated on 2015-12-12 16:08:15

#include "Arduino.h"
#include <Arduino.h>
#include <FastLED.h>
#include <TimedAction.h>
#include <EEPROM.h>
#include <Debounce.h>
void setup() ;
void loop() ;
void nextPattern() ;
void timerIsr() ;
void FlashStripe(uint8_t count, CRGB c) ;
void SaveEEProm() ;
void FixAmbient(uint8_t arg0, uint8_t arg1, uint8_t arg2) ;


#include "FarbTherapie_Deckenlampe.ino"
